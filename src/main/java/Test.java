import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Test {


    public static  void main(String[] args) throws InterruptedException {

        //here you will specify driver type and its location
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Tuğçe\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        //Giriş sayfasının açılmasından sonra kontrolü için console'a bir Giriş sayfası açıldı yazılır.
        driver.get("https://www.n11.com/giris-yap");
        System.out.println("Giriş sayfası açıldı");

        //Giriş sayfasında üye için email ve password bilgileri girilerek Üye Girişi butonuna basılır.
        driver.findElement(By.id("email")).sendKeys("tugce.albayrak@ogr.sakarya.edu.tr");
        driver.findElement(By.name("password")).sendKeys("tugce.albayrak1");
        driver.findElement(By.id("loginButton")).click();

        //Sayfa açıldıktan sonra arama kısmına samsung yazılarak arama sonuçları listelenir.
        driver.findElement(By.id("searchData")).sendKeys("samsung");
        driver.findElement(By.id("searchData")).sendKeys(Keys.ENTER);
        System.out.println("samsung için sonuçlar listelendi");


	    /* 5.adım eksiktir çünkü arama sonuçlarındaki 2.sayfaya gidilmesi gerekirken,1.sayfadaki
	     * listelenen arama sonuçlarından ilki baz alınarak favorilere ekle linkine tıklanılır ve onaylanır.*/
        String linkPath1="//div[@class='proDetail']/span";
        driver.findElement(By.xpath(linkPath1)).click();
        System.out.println("1.arama sayfasındaki ilk sonuçta favorilere ekle tıklanıldı");

        //Arama sayfasındaki ilk sonuca tıklanılır ve açılan sayfa bir süre ekranda durur.
        String pathFirst="//div[@class='proDetail']/a";
        driver.findElement(By.xpath(pathFirst)).click();
        Thread.sleep(2000);

        //Açılan sayfada Hesabım linkine tıklanır ve açılan sayfa bir süre ekranda durur.
        String linkUser="//a[@class='menuLink user']";
        driver.findElement(By.xpath(linkUser)).click();
        Thread.sleep(2000);

	    /*Bu adımda eksiklik vardır çünkü açılan sayfada İstek Listelerim linkinin açılması istenirken
	     * Mevcut Sipariş linki açılmaktadır.*/
        String linkFav="//div[@class='accMenu-cover']//div[@class='accNav']/ul//li[@class='active']/a";
        driver.findElement(By.xpath(linkFav)).click();

        //Sayfa bir süre sonra kapatılır.
        Thread.sleep(5000);
        driver.quit();


    }
}
